
# Consider dependencies only in project.
set(CMAKE_DEPENDS_IN_PROJECT_ONLY OFF)

# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  )

# The set of dependency files which are needed:
set(CMAKE_DEPENDS_DEPENDENCY_FILES
  "/Users/jayantbhatia/eecs3550_sp2024_a05_bhatia/src/Dice.cpp" "tests/CMakeFiles/unit_tests.dir/__/src/Dice.cpp.o" "gcc" "tests/CMakeFiles/unit_tests.dir/__/src/Dice.cpp.o.d"
  "/Users/jayantbhatia/eecs3550_sp2024_a05_bhatia/src/Die.cpp" "tests/CMakeFiles/unit_tests.dir/__/src/Die.cpp.o" "gcc" "tests/CMakeFiles/unit_tests.dir/__/src/Die.cpp.o.d"
  "/Users/jayantbhatia/eecs3550_sp2024_a05_bhatia/src/Game.cpp" "tests/CMakeFiles/unit_tests.dir/__/src/Game.cpp.o" "gcc" "tests/CMakeFiles/unit_tests.dir/__/src/Game.cpp.o.d"
  "/Users/jayantbhatia/eecs3550_sp2024_a05_bhatia/src/ScoreCard.cpp" "tests/CMakeFiles/unit_tests.dir/__/src/ScoreCard.cpp.o" "gcc" "tests/CMakeFiles/unit_tests.dir/__/src/ScoreCard.cpp.o.d"
  "/Users/jayantbhatia/eecs3550_sp2024_a05_bhatia/tests/unit_tests.cpp" "tests/CMakeFiles/unit_tests.dir/unit_tests.cpp.o" "gcc" "tests/CMakeFiles/unit_tests.dir/unit_tests.cpp.o.d"
  )

# Targets to which this target links which contain Fortran sources.
set(CMAKE_Fortran_TARGET_LINKED_INFO_FILES
  )

# Targets to which this target links which contain Fortran sources.
set(CMAKE_Fortran_TARGET_FORWARD_LINKED_INFO_FILES
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")

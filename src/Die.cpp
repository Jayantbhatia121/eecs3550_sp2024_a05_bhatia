#include "Die.h"

// Constructor
Die::Die() {
    roll(); // Roll the die when it's created
}

// Roll the die
void Die::roll() {
    value = rand() % 6 + 1; // Generate a random value between 1 and 6
}

// Get the value of the die
int Die::getValue() const {
    return value;

}

void Die::setValues(int newValue) {
    if (newValue >= 1 && newValue <= 6) {
        value = newValue;
    } else {
        // Handle error, maybe throw an exception
    }
}

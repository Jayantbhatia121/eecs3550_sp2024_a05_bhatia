#include "Game.h"

// Constructor
Game::Game() : dice(5), scorecard(13) {}

// Method to start playing the game
void Game::play() {
    std::string AllCategories[13] = { "Ones", "Twos", "Threes", "Fours", "Fives", "Sixes",
                                      "Three of a Kind", "Four of a Kind", "Full House", "Small Straight", "Large Straight",
                                      "Yahtzee", "Chance" };

    srand(time(0));

    while (!allCategoriesFilled()) {
        dice.rollAll();

        std::cout << "Your values after the dice roll" << std::endl;
        for (int i = 0; i < 5; ++i) {
            std::cout << "Die " << i + 1 << " is " << dice.getValue(i) << std::endl;
        }
        std::cout << "\n";

        int chosenCategory;
        bool validInput = false;

        do {
            std::cout << "Choose a category to score:\n";
            std::cout << "___________\n";
            for (int i = 0; i < 13; ++i) {
                std::cout << " " << i << " - " << AllCategories[i] << std::endl;
            }
            std::cout << "__________\n";
            std::cout << "Your chosen category is: ";

            if (!(std::cin >> chosenCategory)) {
                std::cerr << "Invalid input. Exiting the program." << std::endl;
                exit(EXIT_FAILURE);
            }

            if (chosenCategory < 0 || chosenCategory > 12) {
                std::cerr << "Invalid category. Please choose a category between 0 and 12." << std::endl;
            } else if (scorecard.getScore(chosenCategory) != -1) {
                std::cerr << "Category already filled. Please choose another category." << std::endl;
            } else {
                validInput = true;
            }
        } while (!validInput);

        int score = calculateScore(chosenCategory); // Calculate score for the chosen category
        scorecard.setScore(chosenCategory, score); // Set the score for the chosen category

        std::cout << "\nUpdated Scorecard:\n";
        for (int i = 0; i < 13; ++i) {
            if (scorecard.getScore(i) != -1) {
                std::cout << AllCategories[i] << ": " << scorecard.getScore(i) << std::endl;
            } else {
                std::cout << AllCategories[i] << ": -" << std::endl;
            }
        }
        std::cout << "---------------------\n";
    }

    displayFinalScore();
}

// Check if all categories in the scorecard are filled
bool Game::allCategoriesFilled() {
    for (int i = 0; i < 13; ++i) {
        if (scorecard.getScore(i) == -1) {
            return false;
        }
    }
    return true;
}

// Calculate the score for a given category
int Game::calculateScore(int category) {
    int score = 0;
    bool three = false;
    bool two = false;
    bool straight = true;

  switch (category)
	  {
	  case 0:
		for (int i = 0; i < 5; ++i)
		  {
			if (dice.getValue (i) == 1)
			  {
				score += 1;
			  }
		  }
		break;
	  case 1:
		for (int i = 0; i < 5; ++i)
		  {
			if (dice.getValue (i) == 2)
			  {
				score += 2;
			  }
		  }
		break;
	  case 2:
		for (int i = 0; i < 5; ++i)
		  {
			if (dice.getValue (i) == 3)
			  {
				score += 3;
			  }
		  }
		break;
	  case 3:					// Fours
		for (int i = 0; i < 5; ++i)
		  {
			if (dice.getValue (i) == 4)
			  {
				score += 4;
			  }
		  }
		break;
	  case 4:					// Fives
		for (int i = 0; i < 5; ++i)
		  {
			if (dice.getValue (i) == 5)
			  {
				score += 5;
			  }
		  }
		break;
	  case 5:					// Sixes
		for (int i = 0; i < 5; ++i)
		  {
			if (dice.getValue (i) == 6)
			  {
				score += 6;
			  }
		  }
		break;
	  case 6:
		for (int value = 1; value <= 6; ++value)
		  {
			int count = 0;
			for (int i = 0; i < 5; ++i)
			  {
				if (dice.getValue (i) == value)
				  {
					count++;
				  }
			  }
			if (count >= 3)
			  {
				// Calculate the sum of all dice
				for (int i = 0; i < 5; ++i)
				  {
					score += dice.getValue (i);
				  }
				break;
			  }
		  }
		break;
	  case 7:					// Four of a Kind
		// Check if there are at least four dice with the same value
		for (int value = 1; value <= 6; ++value)
		  {
			int count = 0;
			for (int i = 0; i < 5; ++i)
			  {
				if (dice.getValue (i) == value)
				  {
					count++;
				  }
			  }
			if (count >= 4)
			  {
				// Calculate the sum of all dice
				for (int i = 0; i < 5; ++i)
				  {
					score += dice.getValue (i);
				  }
				break;
			  }
		  }
		break;
	  case 8:					// Full House
		// Check if there is a three of a kind and a pair
		for (int value = 1; value <= 6; ++value)
		  {
			int count = 0;
			for (int i = 0; i < 5; ++i)
			  {
				if (dice.getValue (i) == value)
				  {
					count++;
				  }
			  }
			if (count == 3)
			  {
				three = true;
			  }
			if (count == 2)
			  {
				two = true;
			  }
		  }
		if (three && two)
		  {
			score = 25;
		  }
		break;
	  case 9: //small
		for (int i = 1; i <= 3; ++i)
		  {
			bool straight = true;
			for (int j = i; j < i + 4; ++j)
			  {
				bool found = false;
				for (int k = 0; k < 5; ++k)
				  {
					if (dice.getValue (k) == j)
					  {
						found = true;
						break;
					  }
				  }
				if (!found)
				  {
					straight = false;
					break;
				  }
			  }
			if (straight)
			  {
				score = 30;
				break;
			  }
		  }
		break;
	  case 10: //large
		for (int i = 1; i <= 5; ++i)
		  {
			bool found = false;
			for (int j = 0; j < 5; ++j)
			  {
				if (dice.getValue (j) == i)
				  {
					found = true;
					break;
				  }
			  }
			if (!found)
			  {
				straight = false;
				break;
			  }
		  }
		if (straight)
		  {
			score = 40;
		  }
		break;
	  case 11:					// Yahtzee
		// Check if all dice have the same value
		for (int i = 1; i <= 6; ++i)
		  {
			int count = 0;
			for (int j = 0; j < 5; ++j)
			  {
				if (dice.getValue (j) == i)
				  {
					count++;
				  }
			  }
			if (count == 5)
			  {
				score = 50;
				break;
			  }
		  }
		break;
	  case 12:					// Chance
		// Sum of all dice
		for (int i = 0; i < 5; ++i)
		  {
			score += dice.getValue (i);
		  }
		break;
	  }
	return score;
  }


// Display the final score
void Game::displayFinalScore() {
    std::cout << "\nThe Final Score is: " << scorecard.getTotalScore() << std::endl;
}
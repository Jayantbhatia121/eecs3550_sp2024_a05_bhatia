#ifndef GAME_H
#define GAME_H

#include <iostream>
#include <cstdlib> // for exit()
#include <ctime> // for time()
#include "Dice.h" // Include the Dice class header file
#include "ScoreCard.h" // Include the ScoreCard class header file

class Game {

    
public:
    Dice dice;
    ScoreCard scorecard;        
    Game(); // Constructor
    void play(); // Method to start playing the game
    bool allCategoriesFilled(); // Check if all categories in the scorecard are filled
    int calculateScore(int category); // Calculate the score for a given category
    void displayFinalScore(); // Display the final score
    int getTotalScore() const;
};

#endif // GAME_H
